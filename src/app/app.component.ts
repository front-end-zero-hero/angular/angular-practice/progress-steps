import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements  AfterViewInit {
  title = 'progress-steps';
  circles: number[] = [1,2,3,4];
  private currentStepSubject = new BehaviorSubject<number>(1);
  currentStepAction$ = this.currentStepSubject.asObservable();
  currentActive: number = 1;
  @ViewChild('progress', {static: true}) progress!: ElementRef<HTMLDivElement>;

  constructor() {
    this.currentStepAction$.subscribe((value) => {
      this.currentActive = value;
      if (this.progress) {
        this.onProgressUpdate();
      }
    })
  }

  ngAfterViewInit(): void {
    this.onProgressUpdate();
  }

  onClickNext = () => {
    let currentStep =  this.currentStepSubject.getValue()
    if(currentStep === this.circles.length)
    {
      currentStep = this.circles.length;
      this.currentStepSubject.next(currentStep);
      return;
    }
    currentStep += 1;
    this.currentStepSubject.next(currentStep);
  }

  onClickPrev = () => {
    let currentStep =  this.currentStepSubject.getValue()
    if(currentStep === 1)
    {
      currentStep = 1;
      this.currentStepSubject.next(currentStep);
      return;
    }
    currentStep -= 1;
    this.currentStepSubject.next(currentStep);
  }

  onProgressUpdate = () => {
    this.progress.nativeElement.style.width = `${this.calculateWidth(this.currentActive)}%`;
  }

  get isPrevButtonActive() {
      return this.currentActive > 1;
  }

  get isNextButtonActive():boolean {
      return this.currentActive < 4;
  }

  calculateWidth = (currentActive: number): number => {
    return ((currentActive - 1) / (this.circles.length - 1)) * 100;
  }
}
